const path = require('path');
const webpack = require('webpack');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');

module.exports = {
	entry: './src/main.js',
	output: {
		path: path.resolve(__dirname, './dist'),
		publicPath: 'dist/',
		filename: 'build.js'
	},
	module: {
		rules: [
		{
			test: /\.scss$/,
			use: [
			'style-loader',
			'css-loader',
			'autoprefixer-loader',
			'sass-loader',
			// {
			// 	loader: 'sass-resources-loader',
			// 	options: {
			// 		resources: [
			// 		path.resolve(__dirname, './src/styles/_main.scss')
			// 		]
			// 	}
			// }
			]
		},
		{
			test: /\.styl$/,
			use: [
			'style-loader',
			'css-loader',
			'stylus-loader'
			]
		},
		{
			test: /\.css$/,
			use: [
			'style-loader',
			'css-loader'
			]
		},
		{ 
			test: /\.vue$/,
			loader: 'vue-loader',
			options: {
				loaders: {
					scss: 'vue-style-loader!css-loader!autoprefixer-loader!sass-loader'
				}
			}
		},
		{
			test: /\.js$/,
			loader: 'babel-loader',
		},
		
		]
	},
	plugins: [
	// new webpack.DefinePlugin({
	// 	'process.env': {
	// 		NODE_ENV: '"production"'
	// 	}
	// }),
	// new UglifyJSPlugin()
	]
}