<script>
render: function (h) {
	const self = this;
	let tabNames = this.tabs.map(item => {
		return h('li',
		{
			class: 'tabs__button',
			domProps: {
				innerHTML: item
			},
			_category: item,
			attrs: {
				'data-target': item
			},
			on: {
				click: self.selectTab
			}
		})
	}),
	slots = this.tabs.map(item => {
		return h('div',
		{
			class: 'tabs__item',
			attrs: {
				'data-tab': item
			}
		},
		[this.$slots[item]])
	});
	return h('div', 
	         {class: 'tabs'}, 
	         [h('ul',{class: 'tabs__head'}, tabNames),
	         h('div',{class: 'tabs__body'}, slots)]
	         )
},

</script>