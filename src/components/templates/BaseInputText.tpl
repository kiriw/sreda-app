<template>
	<div class="input">
		<input class="input__input"
		type="text"
		v-model="inputValue"
		@input="input"
		>
		<div
		class="input__reset"
		v-if="inputValue && btnDel"
		@click="reset"
		>&times;</div>
	</div>
</template>

render: function(createElement) {
		let self = this;
		return createElement(
			'div',{
				class: 'input'
			}, [
			createElement('input', {
				class: 'input__input',
				domProps: {
					value: self.inputValue
				},
				on: {
					input: function(event) {
						self.inputValue = event.target.value;
						self.$emit('input', event.target.value)
					}
				}
			}),
			createElement('div', {
				class: 'input__reset',
				domProps: {
					innerHTML: '&times;'
				},
				on: {
					click: self.reset
				}
			})
			])
	},