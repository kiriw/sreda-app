import Vue from 'vue'
import App from './App.vue'
import './styles/_main.scss'

import ModalWindow from './components/ModalWindow.vue'
import BaseInputText from './components/BaseInputText.vue'
import BaseDropdown from './components/BaseDropdown.vue'
import InputSearchQuery from './components/InputSearchQuery.vue'

// import Vuetify from 'vuetify/src'

// Vue.use(Vuetify)

Vue.component('ModalWindow', ModalWindow);
Vue.component('BaseInputText', BaseInputText);
Vue.component('BaseDropdown', BaseDropdown);
Vue.component('InputSearchQuery', InputSearchQuery);




Vue.component('InputSearchQuery', InputSearchQuery);

new Vue({
  el: '#app',
  render: h => h(App),
})