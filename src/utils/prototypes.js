Array.prototype.createTree = function(id, parentId, children) {
	let result = {};

	
	function compare(hash, item) {
		if (item[parentId] == hash[id]) {
			if (!hash[children]) hash[children] = [];
			hash[children].push(item);
			return true
		}
		if (hash[children]) {
			hash[children].forEach(getProp)
		} else {
			return false
		}
	}
	
	this.forEach((item, id) => {
		if (!item[parentId]) {
			result[item.id] = item;
			this.splice(id,1);
		}
	});
	let i = 0;
	while (this.length > 0) {
		for (key in result) {
			compare(result[key], this[i]) && this.splice(i,1);
		}
		
		i++;
	}
	return result;
}