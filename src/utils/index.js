import prototypes from './prototypes'

function prop(name) {
	return function(object) {
		return object[name];
	}
}




function propById(arr, id, prop) {
	return arr.filter(item => item.id == id)[0][prop]
}

export default {
	propById: propById
}