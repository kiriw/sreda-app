let data = [{
	title: 'Пункт 1',
	id: 0,
	selected: 2,
	list: [
	{name: 'Отключены', id: 0},
	{name: 'Все', id: 1},
	{name: 'Только важные', id: 2}
	]
},{
	title: 'Пункт 2',
	id: 1,
	selected: 1,
	list: [
	{name: 'Иван', id: 0},
	{name: 'Петров', id: 1}
	]
},{
	title: 'Пункт 1',
	id: 2,
	selected: 2,
	list: [
	{name: 'Отключены', id: 0},
	{name: 'Все', id: 1},
	{name: 'Только важные', id: 2}
	]
},{
	title: 'Пункт 2',
	id: 3,
	selected: 1,
	list: [
	{name: 'Иван', id: 0},
	{name: 'Петров', id: 1}
	]
}];


function save(values) {
	return new Promise((resolve, reject) => {
		setTimeout(function() {
			data = values;
			resolve();
		}, 800)
	})
}

function load() {
	return new Promise((resolve, reject) => {
		setTimeout(function() {
			resolve(data);
		}, 1000)
	})
}

export default {
	save: save,
	load: load
}